package com.cpubrew.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

public class ScrollBar extends Component implements MouseListener {

	private ScrollPane scrollPane;
	private ScrollState currentState;
	private int barWidth = 15;
	private boolean mouseDown;
	private int pressY;
	private int barY;
	private int unitIncrement = 16;
	private int blockIncrement;
	
	public ScrollBar() {
		addMouseListener(this);
		setRenderBackground(true);
		changeState(ScrollState.IDLE);
		
		setMinWidth(barWidth);
		setMaxWidth(barWidth);
		setSize(barWidth, 0);
	}
	
	@Override
	public void update(float delta) {
		if(currentState == ScrollState.SCROLL) {
			if(!MouseManager.isPressed()) {
				changeState(ScrollState.IDLE);
			}
			int dy = MouseManager.y - pressY;
			setBarPosition(barY + dy);
		}
	}

	@Override
	public void render(SpriteBatch batch) {
	}

	@Override
	public void onMouseDown(MouseEvent ev) {
		mouseDown = true;
		pressY = MouseManager.y;
		barY = getY();
		changeState(ScrollState.SCROLL);
	}
	
	@Override
	public void onMouseDrag(MouseEvent ev) {
		changeState(ScrollState.SCROLL);
	}

	@Override
	public void onMouseEnter(MouseEvent ev) {
		if(!mouseDown) changeState(ScrollState.HOVER);
	}

	@Override
	public void onMouseExit(MouseEvent ev) {
		if(!mouseDown) changeState(ScrollState.IDLE);
	}
	
	private void changeState(ScrollState newState) {
		if(currentState == newState) return;
		currentState = newState;
		
		switch(currentState) {
		case HOVER:
			setBackgroundColor(Color.GRAY);
			break;
		case IDLE:
			setBackgroundColor(Color.LIGHT_GRAY);
			break;
		case SCROLL:
			setBackgroundColor(Color.DARK_GRAY);
			break;
		}
	}

	public void unitScroll(boolean up) {
		scroll(up ? -unitIncrement : unitIncrement);
	}
	
	public void blockScroll(boolean up) {
		scroll(up ? -blockIncrement : blockIncrement);
	}

	private void scroll(int pixels) {
		int range = scrollPane.getHeight() - height;
		int scrollArea = scrollPane.getScrollAreaHeight() - scrollPane.getHeight();
		
		// px/sA = x/range
		// x = (px * range) / sA
		
		setBarPosition(getY() -(int)((pixels * range) / (float)scrollArea));
	}
	
	public float getScrollPercent() {
		int height = getHeight();
		int scrollPaneHeight = scrollPane.getHeight();
		
		int range = scrollPaneHeight - height;
		int scrollAmount = scrollPaneHeight - (getY() + height);
		
		return scrollAmount / (float) range;
	}
	
	public enum ScrollState {
		IDLE,
		HOVER,
		SCROLL
	}
	
	public void setUnitIncrement(int unitIncrement) {
		this.unitIncrement = unitIncrement;
	}
	
	public int getUnitIncrement() {
		return unitIncrement;
	}
	
	public void setBlockIncrement(int blockIncrement) {
		this.blockIncrement = blockIncrement;
	}
	
	public int getBlockIncrement() {
		return blockIncrement;
	}
	
	protected void setScrollPane(ScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
	
	private void setBarPosition(int y) {
		setPosition(getX(), MathUtils.clamp(y, 0, scrollPane.getHeight() - getHeight()));
	}
	
	@Override
	public void onMouseMove(MouseEvent ev) {
	}
	
	@Override
	public void onMouseUp(MouseEvent ev) {
	}
}
