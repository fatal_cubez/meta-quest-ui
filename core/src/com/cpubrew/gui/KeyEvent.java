package com.cpubrew.gui;

public class KeyEvent {

	private Component source;
	private int key = -1;
	private char character;
	
	public KeyEvent(Component source, int key) {
		this.source = source;
		this.key = key;
	}

	public KeyEvent(Component source, char character) {
		this.source = source;
		this.character = character;
	}
	
	public char getCharacter() {
		return character;
	}
	
	public int getKey() {
		return key;
	}
	
	public Component getSource() {
		return source;
	}
	
}
