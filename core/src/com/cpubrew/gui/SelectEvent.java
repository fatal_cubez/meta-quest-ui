package com.cpubrew.gui;

public class SelectEvent {

	private Component source;
	private boolean select;
	
	public SelectEvent(Component source, boolean select) {
		this.source = source;
		this.select = select;
	}
	
	public boolean isSelect() {
		return select;
	}
	
	public Component getSource() {
		return source;
	}
	
}
