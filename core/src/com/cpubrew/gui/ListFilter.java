package com.cpubrew.gui;

public interface ListFilter<T> {

	public boolean passesFilter(String search, T element);
	
}
