package com.cpubrew.main;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cpubrew.examples.Examples;
import com.cpubrew.gui.UIManager;

public class GdxMain extends ApplicationAdapter {
	
	public static final int WIDTH = 1280;
	public static final int HEIGHT = 720;

	private SpriteBatch batch;
	private OrthographicCamera cam;

	private Examples currentExample;
	
	
	// TODO Add in component listener for resize, added, removed, etc...
	@Override
	public void create () {
		batch = new SpriteBatch();

		cam = new OrthographicCamera();
		cam.setToOrtho(false, WIDTH, HEIGHT);
		
		UIManager.setUICamera(cam);
		Gdx.input.setInputProcessor(UIManager.getInputProcessor());
		
		currentExample = Examples.LIST;
		currentExample.begin();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		UIManager.update(Gdx.graphics.getDeltaTime());
		UIManager.render(batch);
	}
	
	@Override
	public void dispose () {
		currentExample.cleanup();
	}
}
