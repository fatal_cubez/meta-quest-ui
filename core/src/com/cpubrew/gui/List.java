package com.cpubrew.gui;

import java.util.Comparator;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Sort;
import com.cpubrew.gui.layout.LayoutConstraints;
import com.cpubrew.gui.layout.LayoutManager;

public class List<T> extends Container implements ActionListener, HoverListener {

	private Array<T> elements;
	private ListButton<T> selected;
	private int listItemHeight = 25;
	private ListFilter<T> filter;
	
	public List(Array<T> data) {
		backgroundColor = Color.WHITE;
		foregroundColor = Color.BLACK;
		setLayoutManager(new LayoutManager() {
			@Override
			public void layout(Container container) {
				int y = container.getHeight() - listItemHeight;
				for(Component comp : container.getComponents()) {
					if(!comp.isVisible()) continue;
					comp.setPosition(0, y);
					comp.setSize(getWidth(), listItemHeight);
					y -= listItemHeight;
				}
			}
			
			@Override
			public void debugRender(ShapeRenderer shape, Container container) {
				
			}
			
			@Override
			public void add(Component comp, LayoutConstraints constraints) {
			}
		});
		setData(data);
	}
	
	public void setData(Array<T> data) {
		elements = data;
		createButtons();
	}
	
	private void createButtons() {
		int totalHeight = 0;
		removeAll();
		for(T element : elements) {
			ListButton<T> button = new ListButton<T>(element, foregroundColor, backgroundColor);
			button.addListener(this);
			button.setList(this);
			add(button);
			totalHeight += listItemHeight;
		}
		setPrefHeight(totalHeight);
	}
	
	public void addSelectListener(SelectListener listener) {
		listeners.addListener(SelectListener.class, listener);
	}
	
	public void removeSelectListener(SelectListener listener) {
		listeners.removeListener(listener);
	}
	
	public Array<SelectListener> getSelectListener() {
		return listeners.getListeners(SelectListener.class);
	}
	
	public void addHoverListener(HoverListener listener) {
		listeners.addListener(HoverListener.class, listener);
	}
	
	public void removeHoverListener(HoverListener listener) {
		listeners.removeListener(listener);
	}
	
	public Array<HoverListener> getHoverListener() {
		return listeners.getListeners(HoverListener.class);
	}
	
	@Override
	public void onAction(ActionEvent event) {
		@SuppressWarnings("unchecked")
		ListButton<T> source = (ListButton<T>) event.getSource();
		
		// Unselect the old list button
		if(selected != null && !selected.equals(source)) {
			selected.setSelected(false);
		}
		selected = source;
		
		for(SelectListener listener : getSelectListener()) {
			if(source.isSelected()) {
				listener.onSelect(new SelectEvent(source, true));
			} else {
				listener.onDeselect(new SelectEvent(source, false));
			}
		}
	}
	
	public void setFilter(ListFilter<T> filter) {
		this.filter = filter;
	}
	
	public ListFilter<T> getFilter() {
		return filter;
	}
	
	public void updateList(String searchFilter) {
		if(filter == null) return;
		
		// These are all list buttons so we can safely cast
		for(Component comp : getComponents()) {
			@SuppressWarnings("unchecked")
			ListButton<T> button = (ListButton<T>) comp;
			T element = button.getData();

			// If it passes the filter, we set it to be visible
			if(filter.passesFilter(searchFilter, element)) {
				comp.setVisible(true);
			} else {
				comp.setVisible(false);
			}
		}
		updateLayout();
	}
	
	/**
	 * The List should be sorted prior to the user interacting with it. If the sort occurs after user interaction, unpredictable results may occur.
	 */
	public void sort(Comparator<T> comparator) {
		if(comparator == null) return;
		Sort.instance().sort(elements, comparator);
		
		// Reset buttons
		for(int i = 0; i < elements.size; i++) {
			T element = elements.get(i);
			
			@SuppressWarnings("unchecked")
			ListButton<T> button = (ListButton<T>) components.get(i);
			
			button.data = element;
		}
	}
	
	@Override
	public void onHover(HoverEvent event) {
		// Propagate the event
		for(HoverListener listener : getHoverListener()) {
			listener.onHover(event);
		}
	}
	
	public static class ListButton<T> extends Button {
		
		private List<T> list;
		private boolean selected;
		private boolean hovering;
		private BitmapFont font;
		private T data;
		private GlyphLayout layout;
		private Color backColor;
		private Color textColor;
		
		public ListButton(T data, Color textColor, Color backColor) {
			this.data = data;
			this.textColor = textColor;
			this.backColor = backColor;
			layout = new GlyphLayout();
			
			font = new BitmapFont();
			
			addListener(new ActionListener() {
				@Override
				public void onAction(ActionEvent event) {
					setSelected(!selected);
				}
			});
		}
		
		@Override
		public void renderBackground(SpriteBatch batch) {
			batch.end();
			
			shape.setProjectionMatrix(batch.getProjectionMatrix());
			shape.begin(ShapeType.Filled);
			shape.setColor(selected ? textColor : backColor);
			shape.rect(x, y, width, height);
			shape.end();
			
			batch.begin();
		}

		@Override
		public void render(SpriteBatch batch) {
			font.setColor(selected ? backColor : textColor);
			layout.setText(font, data.toString());
			font.draw(batch, layout, x + 5, y + height * 0.5f + layout.height * 0.5f);
		}
		
		public void setSelected(boolean selected) {
			this.selected = selected;
		}
		
		public boolean isSelected() {
			return selected;
		}
		
		public void setHovering(boolean hovering) {
			this.hovering = hovering;
		}
		
		public boolean isHovering() {
			return hovering;
		}
		
		public T getData() {
			return data;
		}
		
		@Override
		public void onMouseEnter(MouseEvent ev) {
			list.onHover(new HoverEvent(this));
		}
		
		public void setList(List<T> list) {
			this.list = list;
		}
		
		public List<T> getList() {
			return list;
		}
		
	}
}
