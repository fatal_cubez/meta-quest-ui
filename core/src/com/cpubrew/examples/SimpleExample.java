package com.cpubrew.examples;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.cpubrew.gui.ActionEvent;
import com.cpubrew.gui.ActionListener;
import com.cpubrew.gui.Button;
import com.cpubrew.gui.Checkbox;
import com.cpubrew.gui.DragHandler;
import com.cpubrew.gui.Label;
import com.cpubrew.gui.TextField;
import com.cpubrew.gui.Window;
import com.cpubrew.main.GdxMain;

public class SimpleExample implements Example {

	private Window window;
	private TextField field;
	private Label label;
	private Button button;
	private Checkbox checkbox;
	
	public SimpleExample() {
		window = new Window("Simple Example");
		
		int y = 80;
		
		BitmapFont font = new BitmapFont();
		font.getData().scale(2.0f);
		
		label = new Label("Enter name: ");
		label.setFont(font);
		label.autoSetSize();
		label.setPosition(0, y);
		label.addMouseListener(new DragHandler());

		field = new TextField();
		field.setFont(font);
		field.setSize(300, 80);
		field.setPosition(label.getX() + label.getWidth() + 5, y);
		
		button = new Button("Press Me");
		button.setFont(font);
		button.setSize(200, 50);
		button.setPosition(label.getX(), field.getY() - button.getHeight() - 10);
		
		checkbox = new Checkbox();
		checkbox.setPosition(button.getX() + button.getWidth() + 10, button.getY());
		checkbox.addActionListener(new  ActionListener() {
			@Override
			public void onAction(ActionEvent event) {
				System.out.println(((Checkbox)event.getSource()).isChecked() ? "Checked!" : "Unchecked!");
			}
		});
		
		window.add(label);
		window.add(field);
		window.add(button);
		window.add(checkbox);
		window.setDebugRender(true);
		
		window.setPosition(GdxMain.WIDTH / 2 - window.getWidth() / 2, GdxMain.HEIGHT / 2 - window.getHeight() / 2);
	}

	@Override
	public void begin() {
		window.setVisible(true);
	}
	
	@Override
	public void end() {
		window.setVisible(false);
	}
	
	@Override
	public void cleanup() {
		window.close();
	}
	
}
