package com.cpubrew.main.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.cpubrew.main.GdxMain;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = GdxMain.WIDTH;
		config.height = GdxMain.HEIGHT;
		new LwjglApplication(new GdxMain(), config);
	}
}
