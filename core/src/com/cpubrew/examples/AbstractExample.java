package com.cpubrew.examples;

import com.cpubrew.gui.Window;

public class AbstractExample implements Example {

	protected Window window;
	
	public AbstractExample() {
		window = new Window();
		window.setSize(1280, 720);
	}
	
	@Override
	public void begin() {
		window.setVisible(true);
	}

	@Override
	public void end() {
		window.setVisible(false);
	}

	@Override
	public void cleanup() {
		window.close();
	}

}
