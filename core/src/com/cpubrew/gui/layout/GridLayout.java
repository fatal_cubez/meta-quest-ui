package com.cpubrew.gui.layout;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ArrayMap;
import com.cpubrew.gui.Component;
import com.cpubrew.gui.Container;

/**
 * Components without constraints will NOT be added to GridLayout.
 * 
 * @author Scott
 *
 */
public class GridLayout implements LayoutManager {

	private int rows;
	private int cols;
	private int padding;
	
	// Global settings for vertical and horizontal gaps
	private int vgap;
	private int hgap;
	
	// Specific settings for vertical and horizontal gaps
	private ArrayMap<Integer, Integer> vgapMap;
	private ArrayMap<Integer, Integer> hgapMap;
	
	// Insets
	private int insetTop;
	private int insetBottom;
	private int insetLeft;
	private int insetRight;
	
	private ArrayMap<Component, GLConstraints> constraintMap;
	
	public GridLayout(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		constraintMap = new ArrayMap<Component, GLConstraints>();
		vgapMap = new ArrayMap<Integer, Integer>();
		hgapMap = new ArrayMap<Integer, Integer>();
	}
	
	@Override
	public void add(Component comp, LayoutConstraints constraints) {
		constraintMap.put(comp, (GLConstraints) constraints);
	}

	@Override
	public void layout(Container container) {
		int cellWidth = getCellWidth(container);
		int cellHeight = getCellHeight(container);
	
		for(Component comp : container.getComponents()) {
			LayoutConstraints constr = constraintMap.get(comp);
			
			// If this component does not have constraint information we hide it
			// NOTE: This may cause some non-obvious behavior if the user forgets to add a constraints object
			if(constr == null) {
				comp.setVisible(false);
				continue;
			} 
			
			GLConstraints constraints = (GLConstraints) constraintMap.get(comp);
			Rectangle bounds = getBoundingBox(comp, constraints, cellWidth, cellHeight);
			comp.setPosition((int)bounds.x, (int)bounds.y);
			comp.setSize((int)bounds.width, (int)bounds.height);
		}
	}
	
	/** Calculates the bounds of a cell. Takes into account hgaps and vgaps */
	private Rectangle getCellBounds(int row, int col, int cellWidth, int cellHeight) {
		// Calculate cumulative spacing up until this cell
		int colSpacing = 0;
		for(int i = 0; i < col; i++) {
			Integer hgap = hgapMap.get(i);
			colSpacing += hgap == null ? this.hgap : hgap;
		}
		
		int rowSpacing = 0;
		for(int i = rows - 2; i >= row; i--) {
			Integer vgap = vgapMap.get(i);
			rowSpacing += vgap == null ? this.vgap : vgap;
		}
		
		int x1 = insetLeft + col * cellWidth + colSpacing;
		int y1 = insetBottom + (rows - row - 1) * cellHeight + rowSpacing;
		
		return new Rectangle(x1, y1, cellWidth, cellHeight);
	}
	
	private Rectangle getBoundingBox(Component component, GLConstraints constraints, int cellWidth, int cellHeight) {
		Rectangle bounds = new Rectangle();
		int row = constraints.getRow();
		int col = constraints.getCol();
		
		Rectangle c1 = getCellBounds(row, col, cellWidth, cellHeight); // Upper Left Cell
		Rectangle c2 = getCellBounds(row + constraints.getRowExt(), col + constraints.getColExt(), cellWidth, cellHeight); // Lower Right Cell
		
		int padLeft   = constraints.getPadLeft()   == -1 ? padding : constraints.getPadLeft();
		int padRight  = constraints.getPadRight()  == -1 ? padding : constraints.getPadRight();
		int padTop    = constraints.getPadTop()    == -1 ? padding : constraints.getPadTop();
		int padBottom = constraints.getPadBottom() == -1 ? padding : constraints.getPadBottom();
		
		int x1 = (int)c1.x + padLeft;
		int y1 = (int)c2.y + padBottom;
		int x2 = (int)(c2.x + c2.width) - padRight;
		int y2 = (int)(c1.y + c1.height) - padTop;
		
		int width = component.getMaxWidth() == 0 ? x2 - x1 : Math.min(component.getMaxWidth(), x2 - x1);
		int height = component.getMaxHeight() == 0 ? y2 - y1 : Math.min(component.getMaxHeight(), y2 - y1);
		
		int x = 0;
		switch(constraints.gethAlign()) {
		case CENTER:
			int cw = x2 - x1;
			x = x1 + (cw / 2 - width / 2);
			break;
		case LEFT:
			x = x1;
			break;
		case RIGHT:
			x = x2 - width;
			break;
		default:
			break;
		}
		
		int y = 0;
		switch(constraints.getvAlign()) {
		case CENTER:
			int ch = y2 - y1;
			y = y1 + (ch / 2 - height / 2);
			break;
		case BOTTOM:
			y = y1;
			break;
		case TOP:
			y = y2 - height;
			break;
		default:
			break;
		}
		
		bounds.x = x;
		bounds.y = y;
		bounds.width = width;
		bounds.height = height;
		return bounds;
	}

	@Override
	public void debugRender(ShapeRenderer shape, Container container) {
		shape.begin(ShapeType.Line);

		Vector2 pos = container.getAbsolutePosition();
		int cw = getCellWidth(container);
		int ch = getCellHeight(container);
		
		shape.setColor(Color.MAGENTA);
		shape.rect(pos.x + insetLeft, pos.y + insetBottom, container.getWidth() - (insetLeft + insetRight), container.getHeight() - (insetTop + insetBottom));

		shape.setColor(Color.GREEN);
		
		for(Component comp : constraintMap.keys()) {
			GLConstraints constraints = constraintMap.get(comp);
			
			// If the component only spans one cell then just draw the cell normally
			if(constraints.getColExt() == 0 && constraints.getRowExt() == 0) {
				Rectangle bounds = getCellBounds(constraints.getRow(), constraints.getCol(), cw, ch);
				shape.rect(pos.x + bounds.x, pos.y + bounds.y, bounds.width, bounds.height);
			}
			// If the component spans multiple cells, then we need two corners to calculate the rendering box
			else {
				Rectangle lowerLeft = getCellBounds(constraints.getRow() + constraints.getRowExt(), constraints.getCol(), cw, ch);
				Rectangle upperRight = getCellBounds(constraints.getRow(), constraints.getCol() + constraints.getColExt(), cw, ch);
				
				int x1 = (int)(lowerLeft.x + pos.x);
				int y1 = (int)(lowerLeft.y + pos.y);
				int x2 = (int)(upperRight.x + upperRight.width + pos.x);
				int y2 = (int)(upperRight.y + upperRight.height + pos.y);
				
				shape.rect(x1, y1, x2 - x1, y2 - y1);
			}
		}
		
		shape.end();
	}

	public void setPadding(int padding) {
		this.padding = padding;
	}
	
	public int getPadding() {
		return padding;
	}
	
	public void setInsets(int left, int right, int top, int bottom) {
		insetLeft = Math.max(left, 0);
		insetRight = Math.max(right, 0);
		insetTop = Math.max(top, 0);
		insetBottom = Math.max(bottom, 0);
	}
	
	public void setHgap(int hgap) {
		this.hgap = hgap;
	}
	
	public int getHgap() {
		return hgap;
	}
	
	public void setVgap(int vgap) {
		this.vgap = vgap;
	}
	
	public int getVgap() {
		return vgap;
	}
	
	private int getCellWidth(Container container) {
		int totalHgap = 0;
		for(int i = 0; i < cols - 1; i++) {
			Integer hgap = hgapMap.get(i);
			totalHgap += hgap == null ? this.hgap : hgap;
		}
		return (container.getWidth() - insetLeft - insetRight - totalHgap) / cols;
	}
	
	private int getCellHeight(Container container) {
		int totalVgap = 0;
		for(int i = 0; i < rows - 1; i++) {
			Integer vgap = vgapMap.get(i);
			totalVgap += vgap == null ? this.vgap : vgap;
		}
		return (container.getHeight() - insetTop - insetBottom - totalVgap) / rows;
	}
	
}
