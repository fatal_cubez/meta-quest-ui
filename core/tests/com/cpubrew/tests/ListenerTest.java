package com.cpubrew.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.utils.Array;
import com.cpubrew.gui.EventListeners;
import com.cpubrew.gui.KeyAdapter;
import com.cpubrew.gui.KeyEvent;
import com.cpubrew.gui.KeyListener;
import com.cpubrew.gui.MouseAdapter;
import com.cpubrew.gui.MouseListener;

public class ListenerTest {

	@Test
	public void listenerTestAddRemove() {
		// Create Listeners
		KeyListener k1 = new KeyAdapter() {
		};
		
		KeyListener k2 = new KeyListener() {
			@Override
			public void onKeyType(KeyEvent ev) {
			}
			
			@Override
			public void onKeyRelease(KeyEvent ev) {
			}
			
			@Override
			public void onKeyPress(KeyEvent ev) {
			}
		};

		MouseAdapter mouse = new MouseAdapter() {
		};
		
		EventListeners listeners = new EventListeners();

		listeners.addListener(KeyListener.class, k1);
		assertEquals(listeners.getNumListeners(), 1);
		
		Array<KeyListener> keyListeners = listeners.getListeners(KeyListener.class);
		assertEquals(keyListeners.size, 1);
		
		listeners.addListener(MouseListener.class, mouse);
		listeners.addListener(KeyListener.class, k2);
		
		keyListeners = listeners.getListeners(KeyListener.class);
		
		assertEquals(keyListeners.size, 2);
		assertEquals(listeners.getListeners(MouseListener.class).size, 1);
		
		listeners.removeListener(k2);
		assertEquals(listeners.getNumListeners(), 2);
		
		keyListeners = listeners.getListeners(KeyListener.class);
		assertEquals(keyListeners.first(), k1);
	}
	
}
