package com.cpubrew.gui.layout;

public class GLConstraints implements LayoutConstraints {


	private int row;
	private int col;
	private int rowExt;
	private int colExt;
	
	private int padTop;
	private int padBottom;
	private int padRight;
	private int padLeft;
	
	private VerticalAlign vAlign;
	private HorizontalAlign hAlign;
	
	public GLConstraints() {
		row = 0;
		col = 0;
		rowExt = 0;
		colExt = 0;
		padTop = -1;
		padBottom = -1;
		padRight = -1;
		padLeft = -1;
		vAlign = VerticalAlign.CENTER;
		hAlign = HorizontalAlign.CENTER;
	}
	
	public void setPosition(int row, int col) {
		setRow(row);
		setCol(col);
	}
	
	public void setExtension(int rowExt, int colExt) {
		setRowExt(rowExt);
		setColExt(colExt);
	}
	
	public void setPadding(int padding) {
		setPadTop(padding);
		setPadBottom(padding);
		setPadRight(padding);
		setPadLeft(padding);
	}
	
	public void setPaddingX(int paddingX) {
		setPadRight(paddingX);
		setPadLeft(paddingX);
	}

	public void setPaddingY(int paddingY) {
		setPadTop(paddingY);
		setPadBottom(paddingY);
	}
	
	public void setAlignment(HorizontalAlign hAlign, VerticalAlign vAlign) {
		sethAlign(hAlign);
		setvAlign(vAlign);
	}
	
	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getRowExt() {
		return rowExt;
	}

	public void setRowExt(int rowExt) {
		this.rowExt = rowExt;
	}

	public int getColExt() {
		return colExt;
	}

	public void setColExt(int colExt) {
		this.colExt = colExt;
	}

	public int getPadTop() {
		return padTop;
	}

	public void setPadTop(int padTop) {
		this.padTop = padTop;
	}

	public int getPadBottom() {
		return padBottom;
	}

	public void setPadBottom(int padBottom) {
		this.padBottom = padBottom;
	}

	public int getPadRight() {
		return padRight;
	}

	public void setPadRight(int padRight) {
		this.padRight = padRight;
	}

	public int getPadLeft() {
		return padLeft;
	}

	public void setPadLeft(int padLeft) {
		this.padLeft = padLeft;
	}

	public VerticalAlign getvAlign() {
		return vAlign;
	}

	public void setvAlign(VerticalAlign vAlign) {
		this.vAlign = vAlign;
	}

	public HorizontalAlign gethAlign() {
		return hAlign;
	}

	public void sethAlign(HorizontalAlign hAlign) {
		this.hAlign = hAlign;
	}
	
	@Override
	public boolean isCorrectType(LayoutManager manager) {
		return manager instanceof GridLayout;
	}
	
	
	public enum VerticalAlign {
		TOP,
		BOTTOM,
		CENTER
	}
	
	public enum HorizontalAlign {
		RIGHT,
		LEFT,
		CENTER
	}
	
	public static class Builder {
		
		private GLConstraints constraints;
		
		public Builder() {
			constraints = new GLConstraints();
		}
		
		public Builder position(int row, int col, int rowExt, int colExt) {
			constraints.setPosition(row, col);
			constraints.setExtension(rowExt, colExt);
			return this;
		}
		
		public Builder position(int row, int col) {
			constraints.setPosition(row, col);
			return this;
		}
		
		public Builder align(HorizontalAlign hAlign, VerticalAlign vAlign) {
			constraints.setAlignment(hAlign, vAlign);
			return this;
		}
		
		public Builder padding(int padding) {
			constraints.setPadding(padding);
			return this;
		}
		
		public Builder padding(int padX, int padY) {
			constraints.setPaddingX(padX);
			constraints.setPaddingY(padY);
			return this;
		}
		
		public Builder padding(int top, int south, int right, int left) { 
			constraints.setPadTop(top);
			constraints.setPadBottom(south);
			constraints.setPadRight(right);
			constraints.setPadLeft(left);
			return this;
		}
		
		public GLConstraints build() {
			return constraints;
		}
		
		
	}

}
