package com.cpubrew.gui.layout;

public interface LayoutConstraints {

	public boolean isCorrectType(LayoutManager manager);
	
}
