package com.cpubrew.gui;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cpubrew.gui.layout.LayoutConstraints;

public class ScrollPane extends Container implements ScrollListener, MouseListener {

	private ScrollBar bar;
	private Component scrollable;
	private boolean scrollListenerAdded = false;
	
	public ScrollPane() {
		setFocusable(true);
		addScrollListener(this);
		addMouseListener(this);

		bar = new ScrollBar();
		bar.setVisible(false);
		bar.setScrollPane(this);
		
		addImpl(bar);
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		
		// Check to see if the mouse cursor is contained in the scrollpane
		// If it is, we want to add a ScrollListener to the MouseManager
		// If it isn't, then we need to remove from the MouseManager
		
		Vector2 pos = getAbsolutePosition();
		Rectangle rect = new Rectangle(pos.x, pos.y, getWidth(), getHeight());
		if(rect.contains(MouseManager.x, MouseManager.y) && !scrollListenerAdded) {
			MouseManager.addScrollListener(this);
			scrollListenerAdded = true;
		} else if(!rect.contains(MouseManager.x, MouseManager.y) && scrollListenerAdded){
			MouseManager.removeScrollListener(this);
			scrollListenerAdded = false;
		}
		
		if(scrollable == null || scrollable.getHeight() < getHeight()) return;
		
		int range = -(getHeight() - scrollable.getHeight());
		int dy = (int)(bar.getScrollPercent() * range);
		
		scrollable.setPosition(0, getHeight() - scrollable.getHeight() + dy);
	}
	
	@Override
	public void onScroll(int amount) {
		bar.unitScroll(amount == -1 ? true : false);
	}

	/**
	 * The component supplied is the one that will be used in the viewport.
	 * @param component
	 */
	public void setScrollable(Component component) {
		if(scrollable != null) {
			remove(scrollable);
		}
		if(component == null) return; 
		scrollable = component;
		addImpl(scrollable);
		scrollable.setPosition(0, getHeight() - component.getHeight()); // Put component in upper left of pane area
		scrollable.setSize(getWidth() - bar.getWidth(), component.getHeight());
		
		updateScrollBar();
	}
	
	private void updateScrollBar() {
		if(scrollable == null) return;
		int contentHeight = scrollable.getHeight();
		int myHeight = getHeight();
		
		// We don't need a scroll bar
		if(myHeight > contentHeight) {
			bar.setVisible(false);
		} else {
			bar.setVisible(true);
		}
		
		float fraction = myHeight / (float)contentHeight;
		int barSize = (int)(fraction * myHeight);
		bar.setSize(bar.getWidth(), barSize);
		bar.setPosition(getWidth() - bar.getWidth(), getHeight() - bar.getHeight());
	}

	@Override
	public void setSize(int width, int height) {
		super.setSize(width, height);
		
		if(scrollable != null){
			scrollable.setPosition(0, getHeight() - scrollable.getHeight()); // Put component in upper left of pane area
			scrollable.setSize(getWidth() - bar.getWidth(), scrollable.getHeight());
		}
		bar.setBlockIncrement(height);
		
		updateScrollBar();
	}
	
	public int getScrollAreaHeight() {
		return scrollable == null ? 0 : scrollable.getHeight();
	}
	
	public void setUnitIncrement(int unitIncrement) {
		bar.setUnitIncrement(unitIncrement);
	}
	
	public void setBlockIncrement(int blockIncrement) {
		bar.setBlockIncrement(blockIncrement);
	}
	
	/**
	 * ADDING TO SCROLLPANE WILL BE IGNORED. Use setScrollable(...) instead.
	 */
	@Override
	public void add(Component component) {
	}

	/**
	 * ADDING TO SCROLLPANE WILL BE IGNORED. Use setScrollable(...) instead.
	 */
	@Override
	public void add(Component component, LayoutConstraints constraints) {
	}

	@Override
	public void onMouseMove(MouseEvent ev) {
	}

	@Override
	public void onMouseDrag(MouseEvent ev) {
	}

	@Override
	public void onMouseUp(MouseEvent ev) {
	}

	@Override
	public void onMouseDown(MouseEvent ev) {
		Rectangle scrollBar = new Rectangle(bar.getX(), 0, bar.getWidth(), getHeight());
		
		if(scrollBar.contains(ev.getX(), ev.getY())) {
			// Scroll Down
			if(ev.getY() < bar.getY()) {
				bar.blockScroll(false);
			} 
			// Scroll Up
			else {
				bar.blockScroll(true);
			}
		}
	}

	@Override
	public void onMouseEnter(MouseEvent ev) {
	}

	@Override
	public void onMouseExit(MouseEvent ev) {
	}
}
