package com.cpubrew.examples;

public enum Examples implements Example {

	SIMPLE {
		SimpleExample example = new SimpleExample();

		@Override
		public void begin() {
			example.begin();
		}

		@Override
		public void end() {
			example.end();
		}

		@Override
		public void cleanup() {
			example.cleanup();
		}
	},
	CLIPPING {
		ClippingExample example = new ClippingExample();
		
		@Override
		public void begin() {
			example.begin();
		}

		@Override
		public void end() {
			example.end();
		}

		@Override
		public void cleanup() {
			example.cleanup();
		}
	},
	LAYOUT {
		LayoutExample example = new LayoutExample();
		
		@Override
		public void begin() {
			example.begin();
		}

		@Override
		public void end() {
			example.end();
		}

		@Override
		public void cleanup() {
			example.cleanup();
		}
	},
	SCROLL {
		ScrollExample example = new ScrollExample();
		
		@Override
		public void begin() {
			example.begin();
		}

		@Override
		public void end() {
			example.end();
		}

		@Override
		public void cleanup() {
			example.cleanup();
		}
	},
	LIST {
		ListExample example = new ListExample();
		
		@Override
		public void begin() {
			example.begin();
		}

		@Override
		public void end() {
			example.end();
		}

		@Override
		public void cleanup() {
			example.cleanup();
		}
	};;

}
