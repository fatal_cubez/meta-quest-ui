package com.cpubrew.gui.layout;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.cpubrew.gui.Component;
import com.cpubrew.gui.Container;

public interface LayoutManager {

	/** LayoutConstraints will always be matching in type with the LayoutManager (cast is safe). */
	public void add(Component comp, LayoutConstraints constraints);
	
	/** This is where the LayoutManager should implement its logic for laying out the components of a container */
	public void layout(Container container);
	
	/** shape.begin() and shape.end() must be used in this method. */
	public void debugRender(ShapeRenderer shape, Container container);
}
