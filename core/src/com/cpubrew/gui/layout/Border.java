package com.cpubrew.gui.layout;

public enum Border {

	TOP,
	BOTTOM,
	RIGHT,
	LEFT,
	CENTER
	
}
