package com.cpubrew.examples;

import com.badlogic.gdx.graphics.Color;
import com.cpubrew.gui.Container;
import com.cpubrew.gui.Label;
import com.cpubrew.gui.TextField;
import com.cpubrew.gui.Window;

public class ClippingExample implements Example {

	private Window window;
	private Container container;
	private Container subContainer;
	private Label label;
	private Label subLabel;
	private TextField field;
	
	public ClippingExample() {
		window = new Window();
		window.setSize(1280, 720);
		window.setDebugRender(true);
		
		container = new Container();
		container.setBackgroundColor(Color.LIGHT_GRAY);
		container.setPosition(400, 300);
		container.setSize(400, 400);

		subContainer = new Container();
		subContainer.setBackgroundColor(Color.GRAY);
		subContainer.setPosition(190, 10);
		subContainer.setSize(200, 200);
		
		label = new Label("This is an absurdly long label for no reason");
		label.setBackgroundColor(Color.WHITE);
		label.setSize(150, 50);
		label.setPosition(30, 20);
		
		field = new TextField();
		field.setSize(300, 20);
		field.setPosition(30, 250);
		
		subLabel = new Label("This is a sub label");
		subLabel.setPosition(10, 180);
		subLabel.setSize(120, 40);
		
		subContainer.add(subLabel);
		
		container.add(label);
		container.add(field);
		container.add(subContainer);
		window.add(container);
	}
	
	@Override
	public void begin() {
		window.setVisible(true);
	}

	@Override
	public void end() {
		window.setVisible(false);
	}

	@Override
	public void cleanup() {
		window.close();
	}
}
