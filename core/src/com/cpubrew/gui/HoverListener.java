package com.cpubrew.gui;

public interface HoverListener extends EventListener {
	
	public void onHover(HoverEvent ev);
	
}
