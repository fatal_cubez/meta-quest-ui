package com.cpubrew.examples;

import com.badlogic.gdx.graphics.Color;
import com.cpubrew.gui.Button;
import com.cpubrew.gui.Container;
import com.cpubrew.gui.ScrollPane;
import com.cpubrew.gui.Window;

public class ScrollExample implements Example {

	private Window window;
	private Container container;
	private ScrollPane scrollPane;
	private Button pressMe;
	
	public ScrollExample() {
		window = new Window();
		window.setSize(1280, 720);
		window.setBackgroundColor(Color.PURPLE);
//		window.setDebugRender(true);
		
		container = new Container();
		container.setSize(200, 400);

		pressMe = new Button("Press Me");
		pressMe.setSize(150, 40);
		pressMe.setPosition(container.getWidth() / 2 - pressMe.getWidth() / 2, 10);
		
		container.add(pressMe);
		
		scrollPane = new ScrollPane();
		scrollPane.setSize(500, 300);
		scrollPane.setPosition(100, 100);
		scrollPane.setScrollable(container);
		
		window.add(scrollPane);
	}
	
	@Override
	public void begin() {
		window.setVisible(true);
	}

	@Override
	public void end() {
		window.setVisible(false);
	}

	@Override
	public void cleanup() {
		window.close();
	}

}
