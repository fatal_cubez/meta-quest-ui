package com.cpubrew.gui;

public interface SelectListener extends EventListener {

	public void onSelect(SelectEvent ev);
	public void onDeselect(SelectEvent ev);
}
