package com.cpubrew.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Array;

public class Checkbox extends Component {

	private static final int BOX_WIDTH = 16;
	private static final int BOX_HEIGHT = 16;
	private static final int BORDER = 2;
	
	private boolean checked = false;
	
	public Checkbox() {
		setSize(BOX_WIDTH, BOX_HEIGHT);
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void onMouseUp(MouseEvent ev) {
				Checkbox box = (Checkbox) ev.getSource();
				box.checked = !box.checked;
				for(ActionListener listener : getActionListeners()) {
					listener.onAction(new ActionEvent(box));
				}
			}
		});
	}
	
	@Override
	public void renderBackground(SpriteBatch batch) {
		batch.end();
		
		Gdx.gl.glEnable(GL20.GL_BLEND);
	    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		
		shape.setProjectionMatrix(batch.getProjectionMatrix());
		shape.begin(ShapeType.Filled);
		shape.setColor(Color.BLACK);
		shape.rect(x, y, BOX_WIDTH, BOX_HEIGHT);
		shape.setColor(Color.WHITE);
		shape.rect(x + BORDER, y + BORDER, BOX_WIDTH - 2 * BORDER, BOX_HEIGHT - 2 * BORDER);
		shape.end();
		
		Gdx.gl.glDisable(GL20.GL_BLEND);
		
		batch.begin();
	}
	
	@Override
	public void update(float delta) {
		
	}

	@Override
	public void render(SpriteBatch batch) {
		if(checked) {
			int checkWidth = BOX_WIDTH / 2;
			int checkHeight = BOX_HEIGHT / 2;
			
			shape.setProjectionMatrix(batch.getProjectionMatrix());
			shape.begin(ShapeType.Filled);
			shape.setColor(Color.BLACK);
			shape.rect(x + BOX_WIDTH / 2 - checkWidth / 2, y + BOX_HEIGHT / 2 - checkHeight / 2, checkWidth, checkHeight);
			shape.end();
		}
	}

	public boolean isChecked() {
		return checked;
	}
	
	public void addActionListener(ActionListener listener) {
		listeners.addListener(ActionListener.class, listener);
	}
	
	public void removeActionListener(ActionListener listener) {
		listeners.removeListener(listener);
	}
	
	public Array<ActionListener> getActionListeners() {
		return listeners.getListeners(ActionListener.class);
	}
}
