package com.cpubrew.gui.layout;

public class BLConstraints implements LayoutConstraints {

	private Border border;
	private Padding padding;
	
	public BLConstraints() {
		this(Border.CENTER);
	}
	
	public BLConstraints(Border border) {
		this(border, null);
	}
	
	public BLConstraints(Border border, Padding padding) {
		this.border = border;
		this.padding = padding;
	}
	
	public Border getBorder() {
		return border;
	}
	
	public void setBorder(Border border) {
		this.border = border;
	}
	
	public void setPadding(Padding padding) {
		this.padding = padding;
	}
	
	public Padding getPadding() {
		return padding;
	}
	
	@Override
	public boolean isCorrectType(LayoutManager manager) {
		return manager instanceof BorderLayout;
	}
	
	
	
}
