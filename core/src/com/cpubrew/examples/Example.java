package com.cpubrew.examples;

public interface Example {

	public void begin();
	public void end();
	public void cleanup();
	
}
