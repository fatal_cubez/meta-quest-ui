package com.cpubrew.gui;

public class HoverEvent {

	private Component source;
	
	public HoverEvent(Component source) {
		this.source = source;
	}
	
	public Component getSource() {
		return source;
	}
	
}
