package com.cpubrew.gui;


import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.HdpiUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.cpubrew.gui.layout.LayoutConstraints;
import com.cpubrew.gui.layout.LayoutManager;

public class Container extends Component {

	protected Array<Component> components;
	private LayoutManager layoutManager;
	
	public Container() {
		components = new Array<Component>();
	}
	
	protected void addImpl(Component component, LayoutConstraints constraints) {
		if(layoutManager == null || !constraints.isCorrectType(layoutManager)) throw new IllegalArgumentException("LayoutConstraints and LayoutManager have mismatching types.");
		layoutManager.add(component, constraints);
		addImpl(component);
	}
	
	protected void addImpl(Component component) {
		component.setParent(this);
		components.add(component);
		updateLayout();
	}
	
	public void add(Component component) {
		addImpl(component);
	}
	
	public void add(Component component, LayoutConstraints constraints) {
		addImpl(component, constraints);
	}
	
	public void remove(Component component) {
		components.removeValue(component, false);
	}
	
	public Array<Component> getComponents() {
		return components;
	}
	
	public void removeAll() {
		components.clear();
	}
	
	@Override
	public void setSize(int width, int height) {
		if(width == this.width && height == this.height) return;
		super.setSize(width, height);
		
		// Update Layout
		updateLayout();
	}
	
	@Override
	public void update(float delta) {
		for(Component comp : components) {
			if(!comp.isEnabled() || !comp.isVisible()) continue;
			comp.update(delta);
		}
	}
	
	@Override
	public void render(SpriteBatch batch) {
		if(!isVisible()) return;
		if(renderingBackground()) renderBackground(batch);

		batch.end();
		
		// If debug rendering is enabled in the Window, then all components are debug rendered
		Window parent = getWindow();
		boolean globalDebugRendering = parent != null && parent.isDebugRender(); 
		
		Matrix4 old = batch.getProjectionMatrix();
		Vector3 translation = new Vector3(x, y, 0);
		old.translate(translation);
		
		batch.setProjectionMatrix(old);
		batch.begin();
		
		for(Component component : components) {
			if(!component.isVisible()) continue;
			
			// START SCISSOR
			Rectangle clip = component.getClipBounds();
			if(clip.width <= 0 || clip.height <= 0) continue;
			
			Camera cam = UIManager.getUICamera();
			Vector3 start = cam.project(new Vector3(clip.x, clip.y, 0));
			Vector3 end = cam.project(new Vector3(clip.x + clip.width, clip.y + clip.height, 0));
			
			HdpiUtils.glScissor((int)start.x, (int)start.y, (int)(end.x - start.x), (int)(end.y - start.y));
			
			if(component.renderingBackground()) component.renderBackground(batch);
			component.render(batch);
			if(component.isDebugRender() || globalDebugRendering) {
				component.debugRender(batch);
			}
			
			batch.flush();
		}
		
		batch.end();
		
		
		old.translate(new Vector3().sub(translation));
		
		batch.setProjectionMatrix(old);
		batch.begin();

		Rectangle myClip = getClipBounds();
		HdpiUtils.glScissor((int)myClip.x, (int)myClip.y, (int)myClip.width, (int)myClip.height);

		if(isDebugRender() || globalDebugRendering) {
			debugRender(batch);
			if(layoutManager != null) {
				batch.end();

				// Debug render the layout manager
				layoutManager.debugRender(shape, this);
				
				batch.begin();
			}
		}
	}
	
	public Component getFirstComponentAt(int x, int y) {
		for(int i = components.size - 1; i >= 0; i--) {
			Component comp = components.get(i);
			if(!comp.isEnabled() || !comp.isVisible()) continue;
			Rectangle bounds = comp.getBounds();
			if(bounds.contains(x, y)) {
				if(comp instanceof Container) {
					return ((Container)comp).getFirstComponentAt(x - comp.getX(), y - comp.getY());
				}
				return comp;
			}
		}
		return this;
	}
	
	public void updateLayout() {
		if(layoutManager != null) {
			layoutManager.layout(this);
		}
	}
	
	public void setLayoutManager(LayoutManager layoutManager) {
		this.layoutManager = layoutManager;
	}
	
	public LayoutManager getLayoutManager() {
		return layoutManager;
	}
	
}
