package com.cpubrew.gui.layout;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.ArrayMap;
import com.cpubrew.gui.Component;
import com.cpubrew.gui.Container;

public class BorderLayout implements LayoutManager {

	private ArrayMap<Border, Component> borderMap;
	private ArrayMap<Component, BLConstraints> constraintMap;
	private ArrayMap<Border, Rectangle> boundsMap;
	private float borderPercent = 0.2f; // What percent of the layout does the border take up (width or height)
	
	// BUG Doesn't take into account padding properly when constructing border regions
	public BorderLayout() {
		borderMap = new ArrayMap<Border, Component>();
		constraintMap = new ArrayMap<Component, BLConstraints>();
		boundsMap = new ArrayMap<Border, Rectangle>();
	}
	
	@Override
	public void add(Component comp, LayoutConstraints constraints) {
		BLConstraints blConstraints = (BLConstraints) constraints;
		
		Border border = blConstraints.getBorder();
		if(borderMap.containsKey(border)) {
			// Remove the component from the constraint map
			constraintMap.removeKey(borderMap.get(border));
		}
		
		// This will override existing border components
		borderMap.put(blConstraints.getBorder(), comp);
		constraintMap.put(comp, blConstraints);
	}

	@Override
	public void layout(Container container) {
		createBounds(container);
		
		for(Border border : boundsMap.keys()) {
			Component comp = borderMap.get(border);
			BLConstraints constraints = constraintMap.get(comp);
			
			Rectangle bounds = boundsMap.get(border);
			Padding padding = constraints.getPadding();
			if(padding != null) {
				bounds = padding.adjust(bounds);
			}
			
			comp.setPosition((int)bounds.x, (int)bounds.y);
			comp.setSize((int)bounds.width, (int)bounds.height);
		}
	}
	
	private void createBounds(Container container) {
		boundsMap.clear();
		int width = container.getWidth();
		int height = container.getHeight();

		// Border element width and height
		int bw = (int)(width * borderPercent);
		int bh = (int)(height * borderPercent);
		
		Component center = borderMap.get(Border.CENTER);
		Component left = borderMap.get(Border.LEFT);
		Component right = borderMap.get(Border.RIGHT);
		Component top = borderMap.get(Border.TOP);
		Component bottom = borderMap.get(Border.BOTTOM);
		
		
		int topHeight    = top    == null ? 0 : (   top.getPrefHeight() != 0 ?    top.getPrefHeight() : bh);
		int bottomHeight = bottom == null ? 0 : (bottom.getPrefHeight() != 0 ? bottom.getPrefHeight() : bh);
		int topBottomHeight = topHeight + bottomHeight;
		
		int leftWidth  = left  == null ? 0 : ( left.getPrefWidth() != 0 ?  left.getPrefWidth() : bw);
		int rightWidth = right == null ? 0 : (right.getPrefWidth() != 0 ? right.getPrefWidth() : bw);
		int leftRightWidth = leftWidth + rightWidth;
		
		int centerWidth = width - leftRightWidth;
		int centerHeight = height - topBottomHeight;

		// Top
		if(top != null) {
			boundsMap.put(Border.TOP, 
					new Rectangle(
							0, 
							height - topHeight, 
							width, 
							topHeight
					));
			
		}
		
		// Bottom
		if(bottom != null) {
			boundsMap.put(Border.BOTTOM, 
					new Rectangle(
							0,
							0,
							width,
							bottomHeight
					));
		}
		
		// Left
		if(left != null) {
			boundsMap.put(Border.LEFT, 
					new Rectangle(
							0, 
							bottomHeight,
							leftWidth, 
							height - topBottomHeight
					));
		}
		
		// Right
		if(right != null) {
			boundsMap.put(Border.RIGHT, 
					new Rectangle(
							width - rightWidth,
							bottomHeight,
							rightWidth, 
							height - topBottomHeight
					));
		}
		
		// Center
		if(center != null) {
			boundsMap.put(Border.CENTER, 
					new Rectangle(
							leftWidth, 
							bottomHeight,
							centerWidth,
							centerHeight
					));
		}
	}
	
	@Override
	public void debugRender(ShapeRenderer shape, Container container) {
//		shape.begin(ShapeType.Line);
//		shape.setColor(Color.CHARTREUSE);
//		
//		shape.end();
	}
	
}
