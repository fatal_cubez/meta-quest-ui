package com.cpubrew.gui;

import com.badlogic.gdx.utils.Array;

public class MouseManager {

	public static int x;
	public static int y;

	private static int button;
	private static boolean pressed;
	private static boolean wasPressed;

	private static Array<ScrollListener> scrollListeners = new Array<ScrollListener>();
	
	public static void addScrollListener(ScrollListener listener) {
		scrollListeners.add(listener);
	}
	
	public static void removeScrollListener(ScrollListener listener) {
		scrollListeners.removeValue(listener, false);
	}
	
	public static Array<ScrollListener> getScrollListeners() {
		return scrollListeners;
	}

	public static int getButton() {
		return button;
	}

	public static boolean isPressed() {
		return pressed;
	}

	public static boolean isJustPressed() {
		return pressed && !wasPressed;
	}

	public static void update() {
		wasPressed = pressed;
	}

	public static void touchDown(int screenX, int screenY, int button) {
		x = screenX;
		y = screenY;
		MouseManager.button = button;
		pressed = true;
	}

	public static void touchUp(int screenX, int screenY, int button) {
		x = screenX;
		y = screenY;
		MouseManager.button = button;
		pressed = false;
	}

	public static void touchDragged(int screenX, int screenY) {
		x = screenX;
		y = screenY;
	}

	public static void mouseMoved(int screenX, int screenY) {
		x = screenX;
		y = screenY;
	}
	
	public static void scrolled(int amount) {
		for(ScrollListener listener : scrollListeners) {
			listener.onScroll(amount);
		}
	}

}
