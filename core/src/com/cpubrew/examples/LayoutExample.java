package com.cpubrew.examples;

import com.badlogic.gdx.graphics.Color;
import com.cpubrew.gui.ActionEvent;
import com.cpubrew.gui.ActionListener;
import com.cpubrew.gui.Button;
import com.cpubrew.gui.Container;
import com.cpubrew.gui.Label;
import com.cpubrew.gui.TextField;
import com.cpubrew.gui.Window;
import com.cpubrew.gui.layout.BLConstraints;
import com.cpubrew.gui.layout.Border;
import com.cpubrew.gui.layout.BorderLayout;
import com.cpubrew.gui.layout.GLConstraints;
import com.cpubrew.gui.layout.GridLayout;
import com.cpubrew.gui.layout.Padding;
import com.cpubrew.gui.layout.GLConstraints.HorizontalAlign;
import com.cpubrew.gui.layout.GLConstraints.VerticalAlign;

public class LayoutExample implements Example {

	private Window window;
	private Container container;
	private TextField field;
	private Label label;
	private Button button1;
	
	// Open Level UI
	private Container openLevelContainer;
	private Label openLevelLabel;
	private Button closeButton;
	private Button openButton;
	private Label nameLabel;
	private TextField nameField;
	
	// Border Layout
	private Container borderContainer;
	private Button leftButton;
	private Button rightButton;
	private Button topButton;
	private Button bottomButton;
	private Button centerButton;
	
	public LayoutExample() {
		window = new Window();
		window.setSize(1280, 720);

		GridLayout layout = new GridLayout(2, 2);
		layout.setPadding(5);
		layout.setInsets(10, 10, 10, 10);
		layout.setHgap(30);
		layout.setVgap(10);
		
		container = new Container();
		container.setSize(500, 200);
		container.setPosition(100, 100);
		container.setLayoutManager(layout);
		
		field = new TextField();
		
		label = new Label("Test Label");
		
		button1 = new Button("Button 1");
		button1.addListener(new ActionListener() {
			@Override
			public void onAction(ActionEvent event) {
				Button button = (Button) event.getSource();
				button.getWindow().setDebugRender(!button.getWindow().isDebugRender());
			}
		});
		
		container.add(field, new GLConstraints.Builder().position(0, 1).build());
		container.add(label, new GLConstraints.Builder().position(0, 0).build());
		container.add(button1, new GLConstraints.Builder().position(1, 0, 0, 1).build());
		
		// Open Level UI
		// ---------------------------------
		
		GridLayout layout2 = new GridLayout(3, 4);
		layout2.setInsets(5, 5, 5, 5);
		layout2.setPadding(5);

		openLevelContainer = new Container();
		openLevelContainer.setSize(450, 220);
		openLevelContainer.setLayoutManager(layout2);
		openLevelContainer.setPosition(650, 100);
		openLevelContainer.setBackgroundColor(Color.DARK_GRAY);
		
		openLevelLabel = new Label("Open Level");
		openLevelLabel.setRenderBackground(false);
		openLevelLabel.setMaxHeight(40);
		openLevelLabel.setMaxWidth(80);
		
		nameLabel = new Label("Name:");
		nameLabel.setRenderBackground(false);
		
		nameField = new TextField();
		nameField.setMaxWidth(150);
		
		openButton = new Button("Open");
		
		closeButton = new Button("Close");
		 
		openLevelContainer.add(openLevelLabel, new GLConstraints.Builder().position(0, 0, 0, 3).align(HorizontalAlign.CENTER, VerticalAlign.TOP).build());
		openLevelContainer.add(nameLabel, new GLConstraints.Builder().position(1, 0).build());
		openLevelContainer.add(nameField, new GLConstraints.Builder().position(1, 1, 0, 2).padding(10, 20).align(HorizontalAlign.RIGHT, VerticalAlign.CENTER).build());
		openLevelContainer.add(openButton, new GLConstraints.Builder().position(2, 0, 0, 1).padding(0, 20, 10, 10).build());
		openLevelContainer.add(closeButton, new GLConstraints.Builder().position(2, 2, 0, 1).padding(0, 20, 10, 10).build());
		
		window.add(container);
		window.add(openLevelContainer);
		
		// Border Layout
		// -------------------------------------
		borderContainer = new Container();
		borderContainer.setPosition(10, 400);
		borderContainer.setSize(500, 280);
		borderContainer.setLayoutManager(new BorderLayout());
		
		topButton = new Button("Top");
		bottomButton = new Button("Bottom");
		rightButton = new Button("Right");
		leftButton = new Button("Left");
		centerButton = new Button("Center");
		
		borderContainer.add(topButton,    new BLConstraints(Border.TOP, new Padding(10, 10, 40, 40)));
		borderContainer.add(bottomButton, new BLConstraints(Border.BOTTOM));
		borderContainer.add(rightButton,  new BLConstraints(Border.RIGHT));
		borderContainer.add(leftButton,   new BLConstraints(Border.LEFT));
		borderContainer.add(centerButton, new BLConstraints(Border.CENTER));
		
		window.add(borderContainer);
	}
	
	@Override
	public void begin() {
		window.setVisible(true);
	}

	@Override
	public void end() {
		window.setVisible(false);
	}

	@Override
	public void cleanup() {
		window.close();
	}

}
