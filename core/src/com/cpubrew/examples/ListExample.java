package com.cpubrew.examples;

import java.util.Comparator;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.cpubrew.gui.ActionEvent;
import com.cpubrew.gui.ActionListener;
import com.cpubrew.gui.Container;
import com.cpubrew.gui.HoverEvent;
import com.cpubrew.gui.HoverListener;
import com.cpubrew.gui.List;
import com.cpubrew.gui.List.ListButton;
import com.cpubrew.gui.ListFilter;
import com.cpubrew.gui.ScrollPane;
import com.cpubrew.gui.SelectEvent;
import com.cpubrew.gui.SelectListener;
import com.cpubrew.gui.TextField;
import com.cpubrew.gui.layout.BLConstraints;
import com.cpubrew.gui.layout.Border;
import com.cpubrew.gui.layout.BorderLayout;

public class ListExample extends AbstractExample {

	private Container main;
	private ScrollPane pane;
	private TextField searchField;
	private List<String> list;
	
	public ListExample() {
		window.setBackgroundColor(Color.PURPLE);
		
		main = new Container();
		main.setLayoutManager(new BorderLayout());
		main.setSize(200, 300);
		main.setPosition(100, 100);
		
		pane = new ScrollPane();
		
		Array<String> data = new Array<String>();
		data.addAll("apple", "pineapple", "blueberry", "banana", "cherry", "orange", "strawberry", "grape", "blackberry", "grapefruit", "dragon fruit", "papaya", "mango", "pomegranate", "tangerine", "kumquat");
		list = new List<String>(data);
		list.addSelectListener(new SelectListener() {
			@Override
			public void onSelect(SelectEvent ev) {
				@SuppressWarnings("unchecked")
				ListButton<String> button = (ListButton<String>)ev.getSource();
				System.out.println(button.getData());
			}
			
			@Override
			public void onDeselect(SelectEvent ev) {
				
			}
		});
		list.setFilter(new ListFilter<String>() {
			@Override
			public boolean passesFilter(String search, String element) {
				return element.toLowerCase().contains(search.toLowerCase().trim());
			}
		});
		list.sort(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareToIgnoreCase(o2);
			}
		});
		list.addHoverListener(new HoverListener() {
			@Override
			public void onHover(HoverEvent event) {
				System.out.println("Hovering over: " + ((ListButton<String>)event.getSource()).getData());
			}
		});
		list.setSize(list.getWidth(), list.getPrefHeight());
		
		pane.setScrollable(list);
		
		searchField = new TextField();
		searchField.setPrefHeight(30);
		searchField.addActionListener(new ActionListener() {
			@Override
			public void onAction(ActionEvent event) {
				TextField field = (TextField) event.getSource();
				list.updateList(field.getText());
			}
		});
		
		main.add(pane, new BLConstraints(Border.CENTER));
		main.add(searchField, new BLConstraints(Border.TOP));
		
		window.add(main);
		
		searchField.requestFocus();
	}
	
}
