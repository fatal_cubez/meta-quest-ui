package com.cpubrew.gui.layout;

import com.badlogic.gdx.math.Rectangle;

public class Padding {

	private int top;
	private int bottom;
	private int right;
	private int left;
	
	public Padding(int top, int bottom, int right, int left) {
		setTop(top);
		setBottom(bottom);
		setRight(right);
		setLeft(left);
	}

	public Padding() {
		this(0, 0, 0, 0);
	}
	
	public void setPadding(int padding) {
		setTop(padding);
		setBottom(padding);
		setRight(padding);
		setLeft(padding);
	}

	/**
	 * Returns the adjusted bounds after applying padding.
	 * @param bounds
	 * @return
	 */
	public Rectangle adjust(Rectangle bounds) {
		return new Rectangle(
				bounds.x + left,
				bounds.y + bottom,
				bounds.width - (right + left),
				bounds.height - (top + bottom)
		);
	}
	
	public int getTop() {
		return top;
	}
	
	public void setTop(int top) {
		this.top = top;
	}
	
	public int getBottom() {
		return bottom;
	}
	
	public void setBottom(int bottom) {
		this.bottom = bottom;
	}
	
	public int getRight() {
		return right;
	}
	
	public void setRight(int right) {
		this.right = right;
	}
	
	public int getLeft() {
		return left;
	}
	
	public void setLeft(int left) {
		this.left = left;
	}

	@Override
	public String toString() {
		return "Padding [top=" + top + ", bottom=" + bottom + ", right=" + right + ", left=" + left + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bottom;
		result = prime * result + left;
		result = prime * result + right;
		result = prime * result + top;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Padding other = (Padding) obj;
		if (bottom != other.bottom) return false;
		if (left != other.left) return false;
		if (right != other.right) return false;
		if (top != other.top) return false;
		return true;
	}
	
}
