package com.cpubrew.gui;

public interface ScrollListener extends EventListener {

	public void onScroll(int amount);
	
}